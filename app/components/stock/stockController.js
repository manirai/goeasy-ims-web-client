goEasyIMSWebClientApp.controller('stockController', function($scope) {
	$scope.additionalStockData = [];
	$scope.gridOptions = {
		data : 'additionalStockData',
		columnDefs : [ {
			field : 'serialNo',
			displayName : '#',
			width : 40
		}, {
			field : 'productDescription',
			displayName : 'Product Description'
		}, {
			field : 'quantity',
			displayName : 'Quantity',
			width : 100
		}, {
			field : 'buyingRate',
			displayName : 'Buying Rate',
			width : 150
		}, {
			field : 'sellingRate',
			displayName : 'Selling Rate',
			width : 150
		} ]
	};

	$scope.addProduct = function() {
		$scope.additionalStockData[$scope.additionalStockData.length] = {
			serialNo : $scope.additionalStockData.length + 1,
			productDescription : $scope.productDescription,
			quantity : $scope.quantity,
			buyingRate : $scope.buyingRate,
			sellingRate : $scope.sellingRate
		};
	};
});