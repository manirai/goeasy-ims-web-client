goEasyIMSWebClientApp.config(function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/');
	$stateProvider.state('overview', {
		url : '/',
		templateUrl : 'assets/pages/overview.html'
	}).state('stock', {
		url : '/stock',
		templateUrl : 'assets/pages/stock.html'
	});
});