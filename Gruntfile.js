module.exports = function(grunt) {
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.initConfig({
		connect : {
			server : {
				options : {
					keepalive: true
				}
			}
		}
	});
};